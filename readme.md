For the first time to setup the repo run the following commands:
========================================
1- open a terminal  
2- npm run start  
3- open another terminal  
4- chmod +x ./setup.sh  
5- ./setup.sh   

then open on the browser: http://localhost:3009/  

**Note: make sure you are not running something on that port, If you use if do the following:  **  
1- go to nodemon.json first and adjust the port variable.  
2- open the setup script and change the port variable to the same one  

After the initial setup you can run the app directly using:  

npm run dev  