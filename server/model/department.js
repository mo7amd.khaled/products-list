const mongoose = require('mongoose');

const department = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    text: true,
  },
});

const Department = mongoose.model('department', department);

module.exports = Department;
