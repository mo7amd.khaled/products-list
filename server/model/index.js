const mongoose = require('mongoose');
const product = require('./product');
const promotion = require('./promotion');
const department = require('./department');

const connectDB = () => mongoose.connect(process.env.MONGO_DB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  reconnectInterval: 1000,
});
const models = {
  product,
  promotion,
  department,
};
module.exports = {
  connectDB,
  models,
};
