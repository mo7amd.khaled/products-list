const mongoose = require('mongoose');

const promotion = new mongoose.Schema({
  code: {
    type: String,
    required: true,
    unique: true,
    text: true,
  },
  active: {
    type: Boolean,
    required: true,
  },
  discount: {
    type: Number,
    required: true,
    text: true,
  },
});

const Promotion = mongoose.model('promotion', promotion);

module.exports = Promotion;
