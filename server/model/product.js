const mongoose = require('mongoose');


const product = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  price: {
    type: Number,
    required: true,
  },
  departmentName: {
    type: String,
    required: true,
  },
  promotionCode: {
    type: String,
  },
});

const Product = mongoose.model('product', product);

module.exports = Product;
