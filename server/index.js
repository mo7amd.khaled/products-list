const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const {
  connectDB,
  models,
} = require('./model');
const AppController = require('./controller');

const server = express();
const app = next({ dir: '.', dev: true });
const handle = app.getRequestHandler();

const requestGuard = (model, res) => {
  const isAvailable = models[model];
  if (!isAvailable) {
    res.sendStatus(404);
  }
};

module.exports = app.prepare()
  .then(() => {
    connectDB().then(async () => {
      server.use(bodyParser.urlencoded({ extended: true }));
      server.use(bodyParser.json());

      server.get('/', (req, res) => {
        app.render(req, res, '/products', req.params);
      });
      server.get('/api/:model', async (req, res) => {
        requestGuard(req.params.model, res);
        const results = await AppController.index(req.params.model, req.query);
        res.send({
          success: true,
          ...results,
        });
      });
      server.post('/api/:model', async (req, res) => {
        requestGuard(req.params.model, res);
        const results = await AppController.create(req.params.model, req.body);
        res.send({
          success: true,
          data: results,
        });
      });
      server.put('/api/:model', async (req, res) => {
        requestGuard(req.params.model, res);
        const results = await AppController.update(req.params.model, req.query);
        res.send({
          success: true,
          data: results,
        });
      });
      server.delete('/api/:model', async (req, res) => {
        requestGuard(req.params.model, res);
        const results = await AppController.delete(req.params.model, req.query);
        res.send({
          success: true,
          data: results,
        });
      });
      server.get('*', (req, res) => {
        handle(req, res);
      });
    });
    return server;
  })
  .then(() => {
    server.listen(process.env.PORT, () => console.log(`Listing on Port ${process.env.PORT}`));
  })
  .catch((err) => {
    throw new Error(`error on the next server ${err}`);
  });
