const mongoose = require('mongoose');
const { models } = require('../model');

const parseBody = (body) => {
  const newBody = {
    ...body,
  };
  Object.keys(body).forEach((key) => {
    const el = body[key];
    if (el && typeof el === 'string' && el.length > 0 && el[0] === '$') {
      newBody[key] = mongoose.Types.ObjectId(el.split('$')[1]);
    }
  });
  return newBody;
};

const AppController = {
  index(model, query) {
    return new Promise(async (resolve, reject) => {
      try {
        const {
          match = {},
          limit = 10,
          offset = 0,
          fields = { dummy: 0 },
        } = query;
        const addLookups = [
          {
            $lookup: {
              from: 'departments',
              localField: 'departmentName',
              foreignField: 'name',
              as: 'department',
            },
          },
          {
            $lookup: {
              from: 'promotions',
              localField: 'promotionCode',
              foreignField: 'code',
              as: 'promotion',
            },
          },
        ];
        const getData = () => {
          const pipelines = [
            { $match: parseBody(match) },
            { $skip: offset },
            { $limit: limit },
            { $project: fields },
          ];
          if (model === 'product') {
            pipelines.push(...addLookups);
          }
          return models[model].aggregate(pipelines);
        };
        const data = await getData();
        return resolve({
          data,
          query,
        });
      } catch (error) {
        return reject(Error(`error happen on the index method ${error}`));
      }
    });
  },
  create(model, body) {
    return new Promise(async (resolve, reject) => {
      try {
        const parsedBody = parseBody(body);
        const instance = new models[model](parsedBody);
        await instance.save();
        return resolve({
          model,
          instance,
        });
      } catch (error) {
        return reject(Error(`error happen on the create method ${error} ${body}`));
      }
    });
  },
  update(model, query) {
    // TODO: make validation method for each schema to run before the update
    return new Promise((resolve, reject) => {
      try {
        return resolve({
          model,
          query,
        });
      } catch (error) {
        return reject(Error(`error happen on the update method ${error}`));
      }
    });
  },
  delete(model, query) {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await models[model].remove(parseBody(query));
        return resolve({
          data,
          model,
          query,
        });
      } catch (error) {
        return reject(Error(`error happen on the delete method ${error}`));
      }
    });
  },
};
module.exports = AppController;
