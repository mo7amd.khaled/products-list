echo "****************************************************************************************************************"
echo "**************************************initialize setup *********************************************************"
echo "****************************************************************************************************************\n\n\n"
PORT=3009
echo "check if mongo Exists":
echo "=============================="

if pgrep mongod > /dev/null
then
  echo "MongoDB is Running \n"
  echo "start the app...\n\n"
else
  echo "Stopped .. starting MongoDB\n\n"
  docker-compose up
fi

echo Create PromoCodes:
echo "====================="
curl -v --request POST \
  --url "http://localhost:${PORT}/api/promotion" \
  --header 'content-type: application/json' \
  --data '{
	"code": "code789",
	"active": true,
	"discount": 70
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/promotion" \
  --header 'content-type: application/json' \
  --data '{
	"code": "code456",
	"active": true,
	"discount": 50
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/promotion" \
  --header 'content-type: application/json' \
  --data '{
	"code": "code123",
	"active": true,
	"discount": 20
}
'
echo Create Departments:
echo "====================="

curl -v --request POST \
  --url "http://localhost:${PORT}/api/department" \
  --header 'content-type: application/json' \
  --data '{
	"name": "wood"
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/department" \
  --header 'content-type: application/json' \
  --data '{
	"name": "electric"
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/department" \
  --header 'content-type: application/json' \
  --data '{
	"name": "fun"
}
'
echo Create Prodcuts:
echo "====================="

curl -v --request POST \
  --url "http://localhost:${PORT}/api/product" \
  --header 'content-type: application/json' \
  --data '{
	"name": "prodcut test 1",
	"price": 300,
	"departmentName": "electric",
	"promotionCode": "code789" 
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/product" \
  --header 'content-type: application/json' \
  --data '{
	"name": "prodcut test 2",
	"price": 200,
	"departmentName": "wood",
	"promotionCode": "code456" 
}
'
curl -v --request POST \
  --url "http://localhost:${PORT}/api/product" \
  --header 'content-type: application/json' \
  --data '{
	"name": "prodcut test 3",
	"price": 100,
	"departmentName": "fun",
	"promotionCode": "code123" 
}
'
echo "\n\nsetup is Done!\n\n"
exit 1
