import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';

class DepartmentSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      departments,
      onSelectHandler,
    } = this.props;
    return (
      <div>
        <label htmlFor="departments-selector">
          department selector
          {' '}
          <select name="departments-selector" id="departments-selector" onChange={onSelectHandler}>
            <option key="-" value="">-</option>
            {
              map(departments, ({ name }) => (
                <option key={name} value={name}>
                  {name}
                </option>
              ))
            }
          </select>
        </label>
      </div>
    );
  }
}
export default DepartmentSelector;

DepartmentSelector.propTypes = {
  departments: PropTypes.array.isRequired,
  onSelectHandler: PropTypes.func.isRequired,
};
