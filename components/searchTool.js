import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchTool extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      promotion: {
        code = '',
        active = false,
        discount = null,
      } = {},
    } = this.props;
    if (!active || !code || !discount) {
      return false;
    }
    return (
      <div>
        discounted price:
        {' '}
        {discount}
        %
      </div>
    );
  }
}
export default SearchTool;
SearchTool.propTypes = {
  promotion: PropTypes.object,
};

SearchTool.defaultProps = {
  promotion: {},
};
