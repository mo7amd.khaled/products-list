import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PromoCodeBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      promoCode: '',
    };
  }

  onChangeHandler = (e) => {
    this.setState({
      promoCode: e.target.value,
    });
  }

  onSubmitHandler = (e) => {
    e.preventDefault();
    const {
      promoCode,
    } = this.state;
    const {
      applyPromoCode,
    } = this.props;
    if (promoCode !== '') {
      applyPromoCode(promoCode);
    }
  }

  onResetForm = () => {
    const {
      applyPromoCode,
    } = this.props;
    this.setState({
      promoCode: '',
    }, () => {
      applyPromoCode('');
    })
  }

  render() {
    const {
      promoCode,
    } = this.state;
    return (
      <form onSubmit={this.onSubmitHandler}>
        <label htmlFor="promo-box">
          enter promo code
          <input
            type="text"
            name="promo-box"
            id="promo-box"
            value={promoCode}
            onChange={this.onChangeHandler}
          />
        </label>
        <button type="submit">apply promo</button>
        <button type="button" onClick={this.onResetForm}>reset</button>
      </form>
    );
  }
}
export default PromoCodeBox;
PromoCodeBox.propTypes = {
  applyPromoCode: PropTypes.func.isRequired,
};
