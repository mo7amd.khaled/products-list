import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PromotionBadge from './promotionBadge';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      name,
      price,
      promoPrice,
      department,
      promotion,
    } = this.props;
    const priceStyle = promoPrice ? { textDecoration: 'line-through'} : {};
    return (
      <li key={id}>
        <h2>
          name:
          {' '}
          {name}
        </h2>
        <p>
          price:
          {' '}
          <span style={priceStyle}>
            {price}
          </span>
          <span>
            {'  '}
            {promoPrice}
          </span>
        </p>
        <p>
          department:
          {' '}
          {department}
        </p>
        <PromotionBadge promotion={promotion} />
      </li>
    );
  }
}
export default Product;

Product.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  promoPrice: PropTypes.number.isRequired,
  department: PropTypes.string.isRequired,
  promotion: PropTypes.object.isRequired,
};
