import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Product from './product';

class productList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getProductPrice= (price, promotion) => {
    const {
      code,
      active,
      discount,
    } = promotion;
    const {
      currentPromo,
    } = this.props;
    if (code === currentPromo && active) {
      return (price - discount);
    }
    return null;
  }

  render() {
    const {
      products,
      selectedDepartment,
      currentPromo,
    } = this.props;
    return (
      <ul>
        {
          products.map(({
            _id: id,
            name,
            price,
            department,
            promotion,
          }) => {
            const departmentName = department[0].name;
            const promoPrice = this.getProductPrice(price, promotion[0]);
            if (
              (selectedDepartment !== '' && selectedDepartment !== departmentName)
              || (currentPromo !== '' && !promoPrice)
            ) {
              return false;
            }
            return (
              <Product
                key={id}
                id={id}
                name={name}
                price={price}
                department={departmentName}
                promoPrice={promoPrice}
              />
            );
          })
        }
      </ul>
    );
  }
}
export default productList;
productList.propTypes = {
  products: PropTypes.array.isRequired,
  selectedDepartment: PropTypes.string.isRequired,
  currentPromo: PropTypes.string.isRequired,
};
