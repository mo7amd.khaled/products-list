import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PromotionBadge extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      promotion: {
        code = '',
        active = false,
        discount = null,
      } = {},
    } = this.props;
    if (!active || !code || !discount) {
      return false;
    }
    return (
      <div>
        discounted price:
        {' '}
        {discount}
        %
      </div>
    );
  }
}
export default PromotionBadge;
PromotionBadge.propTypes = {
  promotion: PropTypes.object,
};

PromotionBadge.defaultProps = {
  promotion: {},
};
