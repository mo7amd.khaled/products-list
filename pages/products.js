import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';
import qs from 'qs';
import Header from '../components/header';
import ProductList from '../components/productList';
import DepartmentSelector from '../components/departmentSelector';
import PromoCodeBox from '../components/promoCodeBox';

class ProductsList extends Component {
  static async getInitialProps(props) {
    const {
      req,
    } = props;
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    const requestsSchema = [
      {
        endpoint: 'product',
        query: false,
        callback: false,
      },
      {
        endpoint: 'department',
        query: false,
        callback: false,
      },
      {
        endpoint: 'promotion',
        query: false,
        callback: false,
      },
    ];
    const requests = requestsSchema.reduce((all, el) => {
      const {
        endpoint,
        query,
        callback,
      } = el;
      const request = new Promise((resolve, reject) => {
        let url = `${baseUrl}/api/${endpoint}`;
        if (query) {
          url += `?${qs.stringify(query, { indices: false })}`;
        }
        fetch(url, { credentials: 'same-origin' })
          .then((data) => data.json())
          .then(({ data }) => {
            if (callback) {
              return resolve(callback(data));
            }
            return resolve(data);
          })
          .catch((err) => reject(new Error(`error when requesting ${endpoint}, ${err}`)));
      });
      all.push(request);
      return all;
    }, []);
    return Promise.all([
      ...requests,
    ]).then(([products, departments, promotions]) => ({
      products,
      departments,
      promotions,
    }));
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedDepartment: '',
      currentPromo: '',
    };
  }

  onSelectHandler = (e) => {
    this.setState({
      selectedDepartment: e.currentTarget.value,
    });
  }

  applyPromoCode = (code) => {
    this.setState({
      currentPromo: code,
    });
  }

  render() {
    const {
      selectedDepartment,
      currentPromo,
    } = this.state;
    const {
      products,
      departments,
    } = this.props;
    return (
      <>
        <Header />
        <DepartmentSelector
          departments={departments}
          onSelectHandler={this.onSelectHandler}
        />
        <PromoCodeBox applyPromoCode={this.applyPromoCode} />
        <ProductList
          products={products}
          selectedDepartment={selectedDepartment}
          currentPromo={currentPromo}
        />
      </>
    );
  }
}
export default ProductsList;

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  departments: PropTypes.array.isRequired,
  promotions: PropTypes.array.isRequired,
};
